NATIVE=$(shell cc -dumpmachine)
TARGET=i686-apple-darwin9
TARGETDIR=$(PWD)/cross-$(TARGET)
BUILDROOT=$(PWD)
INPUTDIR=$(PWD)/inputs
xPATH=$(PATH):$(TARGETDIR)/bin
xCPLUS_INCLUDE_PATH=$(TARGETDIR)/usr/include:$(TARGETDIR)/usr/include/c++/4.0.0/:$(TARGETDIR)/include/c++/4.0.1/$(TARGET):$(TARGETDIR)/lib/gcc/$(TARGET)/4.0.1/include/

GCCVER=5490

all: did_gcc

fetch:
	mkdir -p $(INPUTDIR)
	$(MAKE) -C $(INPUTDIR) -f ../Makefile.inputs all

$(INPUTDIR)/%:
	mkdir -p $(INPUTDIR)
	$(MAKE) -C $(INPUTDIR) -f ../Makefile.inputs $(subst $(INPUTDIR)/,,$@)

did_cctools: $(INPUTDIR)/odcctools $(INPUTDIR)/odcctools_as.patch $(INPUTDIR)/odcctools_qsort.patch $(INPUTDIR)/odcctools_ld64.patch
	svn export $< odcctools
	$(MAKE) -C odcctools -f ../Makefile _build_cctools
	rm -r odcctools
	touch $@

_build_cctools:
	patch -p0 <$(INPUTDIR)/odcctools_as.patch
	patch -p0 <$(INPUTDIR)/odcctools_qsort.patch
	patch -p0 <$(INPUTDIR)/odcctools_ld64.patch
	./configure --prefix=$(TARGETDIR) --target=$(TARGET) --with-sysroot=$(TARGETDIR) --enable-as-targets=i386 CFLAGS='-O0' --build=$(NATIVE) --host=$(NATIVE)
	sed -i 's/otool//' Makefile
	$(MAKE)
	$(MAKE) install

did_xcode: $(INPUTDIR)/xcode312_2621_developerdvd.dmg
	mkdir xcode
	$(MAKE) -C xcode -f ../Makefile _build_xcode
	rm -r xcode
	touch $@

_build_xcode:
	7z x $(INPUTDIR)/xcode312_2621_developerdvd.dmg 5.hfs
	7z x 5.hfs 'Xcode Tools/Packages/MacOSX10.4.Universal.pkg'
	7z x 'Xcode Tools/Packages/MacOSX10.4.Universal.pkg' Payload
	gzip -d <Payload | cpio -i
	mkdir -p $(TARGETDIR)/
	mv -v SDKs/MacOSX10.4u.sdk/* $(TARGETDIR)/

did_gcc: $(INPUTDIR)/gcc-$(GCCVER).tar.gz $(INPUTDIR)/gcc-32bit.patch $(INPUTDIR)/gcc-cflags.patch $(INPUTDIR)/gcc-tooldir.patch $(INPUTDIR)/gcc-open.patch did_cctools did_xcode
	mkdir gcc-build
	tar xzvpf $<
	$(MAKE) -C gcc-$(GCCVER) -f ../Makefile _prepare_gcc
	PATH=$(xPATH) $(MAKE) -C gcc-build -f ../Makefile _build_gcc
	rm -r gcc-{$(GCCVER),build}
	touch did_gcc

_prepare_gcc:
	patch -p0 <$(INPUTDIR)/gcc-32bit.patch
	patch -p0 <$(INPUTDIR)/gcc-cflags.patch
	patch -p0 <$(INPUTDIR)/gcc-tooldir.patch
	patch -p1 <$(INPUTDIR)/gcc-open.patch

_build_gcc:
	../gcc-$(GCCVER)/configure --prefix=$(TARGETDIR) --disable-checking --enable-languages=c,c++ --with-as=$(TARGETDIR)/bin/$(TARGET)-as --with-ld=$(TARGETDIR)/bin/$(TARGET)-ld --target=$(TARGET) --with-sysroot=$(TARGETDIR) --enable-static --enable-shared --disable-nls --disable-multilib --build=$(NATIVE) --host=$(NATIVE)
	$(MAKE)
	$(MAKE) install
